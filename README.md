# Docker notes
## Table of contents
- [Docker notes](#Docker-notes)
  - [Table of contents](#Table-of-contents)
- [Networking](#Networking)
  - [IPTables](#IPTables)
  - [Multi-host networking](#Multi-host-networking)
- [Volumes](#Volumes)
- [Compose](#Compose)
  - [Configuration](#Configuration)
  - [Lifecycle](#Lifecycle)
  - [Provisioning](#Provisioning)
  - [Monitoring](#Monitoring)
  - [Container control](#Container-control)
  - [Scaling](#Scaling)
- [Frameworks](#Frameworks)
  - [Spring Boot](#Spring-Boot)
  - [Django](#Django)
- [Services](#Services)
  - [MySQL](#MySQL)
    - [Tags](#Tags)
    - [Environment variables](#Environment-variables)
    - [Bootstrapping](#Bootstrapping)
  - [Redis](#Redis)
    - [Tags](#Tags-1)
  - [Mongo](#Mongo)
    - [Command options](#Command-options)
    - [Bootstrapping](#Bootstrapping-1)
- [Docker Machine](#Docker-Machine)
  - [Lifecycle](#Lifecycle-1)
  - [Connecting to the machine](#Connecting-to-the-machine)
  - [Inspecting the machine status](#Inspecting-the-machine-status)
  - [Running containers](#Running-containers)
- [Docker Swarm](#Docker-Swarm)
  - [Creation](#Creation)
- [Monitoring](#Monitoring-1)
  - [Manager lifecycle](#Manager-lifecycle)
- [Services](#Services-1)
- [Stacks](#Stacks)
  - [Definition](#Definition)
- [Lifecycle](#Lifecycle-2)
- [Monitoring](#Monitoring-2)
- [Portainer](#Portainer)
  - [Launch for local](#Launch-for-local)
  - [Launch for swarm](#Launch-for-swarm)

# Networking

```
docker network create <network-name>
```

```
--network <network-name>
--network-alias <host-alias>
```
```
docker network inspect <network-name>
```

## IPTables

Docker uses its own named tables, to add rules affecting the containers, use the DOCKER-USER table.

List rules:
```
iptables -L DOCKER-USER -v --line-numbers
```

Add block rule:
```
iptables -I DOCKER-USER -s <ip-address> -j DROP
```

Delete rule:
```
iptables -D DOCKER-USER <rule-number>
```

## Multi-host networking

[TO DO]

# Volumes

```
docker volume create <volume-name>
docker volume inspect <volume-name>
```

```
--volume <volume-name>:<mount-point>
```

# Compose

## Configuration
```
# Validate the docker-compose.yml file
docker-compose config
```

## Lifecycle

These commands affect all the containers in the stack.

```
# Build and launch the stack (and detach):
docker-compose up -d

# Stop the stack
docker-compose down

# Stop immediately (with possible data loss)
docker-compose kill

# Delete the containers
docker-compose rm

# Stop the stack and delete everything, including volumes
docker-compose down --rmi all --volumes
```

## Provisioning

These actions are run by `docker-compose up` but can be run individually.

```
# Pull the necessary images without running the containers
docker-compose pull

# Build the images without running the containers
docker-compose build

# Create the containers but don't start them
docker-compose create
```


## Monitoring

```
# List the stack containers
docker-compose ps

# Show running processes
docker-compose top [container-name]

# Show the logs
docker-compose [-f] logs [container-name]

# Stream the stack events
docker-compose events
```

## Container control
```
# Control the state of the containers
docker-compose start [container-name]
docker-compose stop [container-name]
docker-compose restart [container-name]
docker-compose pause [container-name]
docker-compose unpause [container-name]

# Run a command in a running container
docker-compose exec <container-name> <command>

# Run a one-off command in a new container
docker-compose run [options] <container-name> <command>
```

## Scaling

Scaling will take care of creating new containers until the requested number is reached.

Containers with exported ports will fail to start if duplicated.

```
# Scaling a running stack
docker-compose up -d --scale <container-name>=<num>
```

# Frameworks

## Spring Boot

https://spring.io/guides/gs/spring-boot-docker/

```
FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

The `/tmp` volume is for Tomcat's working directory.

## Django

```
FROM python:3
ENV PYTHONUNBUFFERED 1
ENV PYTHONIOENCODING UTF-8
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/
```

# Services

## MySQL

https://hub.docker.com/_/mysql

### Tags

* 8.0.16, 8.0, 8, latest
* 5.7.26, 5.7, 5
* 5.6.44, 5.6

### Environment variables

```
MYSQL_ROOT_PASSWORD
MYSQL_USER
MYSQL_PASSWORD
MYSQL_DATABASE
```

Appending `_FILE` to the variable name can be used to read the value from a file:

```
MYSQL_ROOT_PASSWORD_FILE = /run/secrets/mysql_root
```

### Bootstrapping

Leave files with extensions .sh, .sql and .sql.gz in `/docker-entrypoint-initdb.d` and they will be executed in alphabetical order. 

## Redis

https://hub.docker.com/_/redis

### Tags

* redis
* redis:alpine
* redis:<version>
* redis:<version>-alpine

Alpine versions are smaller but use the `musl libc` instead of `glib`.

## Mongo

https://hub.docker.com/_/mongo

### Command options

```
--help          view all available options
--smallfiles    smaller default file size
```

### Bootstrapping

WARNING: this only works if no pre-existing databas is provided

```
MONGO_INITDB_ROOT_USERNAME
MONGO_INITDB_ROOT_PASSWORD
```

Also available with _FILE suffix to load the value form an existing file in the container.

```
MONGO_INITDB_DATABASE
```

Data will be imported from `docker-entrypoint-initdb.d/*.js` when creating the database.

# Docker Machine

Creates and manages virtual and remote hosts to run containers in them.

## Lifecycle
```
docker-machine create --driver <driver> <machine-name>
docker-machine stop <machine-name>
docker-machine start <machine-name>
docker-machine restart <machine-name>
docker-machine rm <machine-name>
```

## Connecting to the machine

```
# Via SSH
docker-machine ssh <machine-name>

# Run command via SSH
docker-machine ssh <machine-name> <command>
```

## Inspecting the machine status

```
docker-machine inspect <machine-name>
docker-machine status <machine-name>
docker-machine ip <machine-name>
docker-machine url <machine-name>
```

## Running containers

```
# List the available machines
docker-machine ls
```

The docker commands will run against the _active_ machine.
The current active machine is determined by environment variables.

```
# Show the variables to set the machine as active
docker-machine env <machine-name>

# Apply the variables for the terminal session
env $(docker machine env <machine-name>)
```

Alternatively, we can specify the options in each docker command to connect to the desired machine:

```
# Show the options needed to connect to a specific machine
docker-machine config <machine-name>

# Use the options when running a docker command
docker $(docker-machine config <machine-name>) container ls
```

# Docker Swarm

## Creation

```
# Init the swarm in the manager node
docker $(docker-machine config <manager-node>) swarm init 
  --advertise-addr $(docker-machine ip <manager-node>):2377 \
  --listen-addr $(docker-machine ip <manager-node>):2377

# Add the workers to the swarm
docker $(docker-machine config <worker-node>) swarm join --token <swarm-token> $(docker-machine ip <manager-node>)
docker $(docker-machine config <worker-node>) swarm join --token <swarm-token> $(docker-machine ip <manager-node)

# Load environment variables so all docker commands are sent to the manager
eval $(docker-machine env <manager-node>)

```

# Monitoring

```
docker node ls
docker node inspect <any-node> --pretty
docker node ps
```

## Manager lifecycle

```
# Promote a node to manager
docker node promote <non-manager-node>

# Demote a node to worker
docker node demote <manager-node>

# Drain a node (stop new tasks and migrate running tasks to other nodes)
docker node update --availability drain <worker-node>

# Re-activate a node
docker node update --availability active swarm-manager
```

# Services
```
docker service create --name <service-name> <docker container run options>
docker service inspect <service-name> --pretty

# Scale the service
docker service scale <service-name>=<number-of-containers>

# Show all the running containers for a service
docker service ps <service-name>

# Redistribute the replicas
docker service update <service-name> --force
```

# Stacks

## Definition

Use a `docker-compose.yml` file.

```
version: "3"

services:
  apache:
    image: httpd:2.4
    ports:
      - "80:80"
    deploy:
      replicas: 4
      restart_policy:
        condition: on-failure
      placement:
        constraints:
          - node.role == worker
```

# Lifecycle
```
# Start
docker stack deploy --compose-file=docker-compose.yml <stack-name>

# Remove
docker stack rm <stack-name>
```

# Monitoring

```
docker stack ls
docker stack services <stack-name>
docker stack ps <stack-name>
```

# Portainer

## Launch for local

```
docker container run \
  --detach
  --publish 9000:9000 
  --volume /var/run/docker.sock:/var/run/docker.sock
  portainer/portainer
```

## Launch for swarm

```
docker $(docker-machine env <manager-node>) service create \
  --name portainer 
  --publish 9000:9000 
  --constraint 'node.role==manager' 
  --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock 
  portainer/portainer 
  -H unix:///var/run/docker.sock

docker service inspect portainer --pretty
```